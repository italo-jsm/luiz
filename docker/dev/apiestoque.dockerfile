FROM mcr.microsoft.com/dotnet/core/sdk:2.2 AS build-env
WORKDIR /app/

COPY ApiEstoque ApiEstoque
COPY Infra Infra

WORKDIR /app/ApiEstoque/

# Copy everything else and build
# COPY . ./
RUN dotnet publish -c Release -o out ApiEstoque.csproj

FROM mcr.microsoft.com/dotnet/core/aspnet:2.2
WORKDIR /app
COPY --from=build-env /app/ApiEstoque/out .
COPY --from=build-env /app/ApiEstoque/EmailsTemplates/email-inlined.html EmailsTemplates/

EXPOSE 5002

ENTRYPOINT ["dotnet", "ApiEstoque.dll"]