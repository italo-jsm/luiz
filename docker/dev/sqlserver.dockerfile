FROM mcr.microsoft.com/mssql/server:2017-CU14-ubuntu

WORKDIR /sqlserver/

ENV ACCEPT_EULA=Y
ENV SA_PASSWORD=root

ENV PATH="/opt/mssql-tools/bin:${PATH}"

COPY . .
