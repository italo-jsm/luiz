Requisitos para que o Backend funcione perfeitamente:
1. Instalar o **Make**
2. Make pode ser instalado facilmente utilizando o Chocolatey (Gerenciador de Pacotes para Windows).
3. Link: https://chocolatey.org/install
4. Após instalado o **Chocolatey**, basta abrir o **CMD** como **Administrador** e digitar "Choco install make"
5. Instale o **Docker**
6. Feito os passos acima, A ordem legal para execução é a seguinte:
7. Abra o **CMD** e navegue até a pasta do projeto e verifique se há um arquivo chamado Makefile, se sim digite o seguinte comando no **CMD** "make build && make run"
8. Ele irá buildar e rodar o postgresql e o pgadmin no docker
9. Senha para acesso ao **PGADMIN** é User: SA / Password: 123456
10. Senha para acesso ao **POSTGRESQL** é User: root / Password: root
11. Feito os passos acima, abra o visual code e execute o seguinte comando "dotnet build && dotnet ef database update"
12. Após o comando acima, digite "dotnet build && dotnet run" e acesse a seguinte url "http://localhost:5002/swagger
13. Para mais informações sobre os comandos Make disponíveis para este projeto consultar o arquivo Makefile dentro da pasta do projeto
