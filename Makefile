# AMBIENTE:=LocalDocker
# NAME:=Initial
# DO_MIGRATION:=false
IMG_VERSION:=1.0-beta
DB_VOLUME_NAME:=postgres-volume

create-network:
	docker network create te || exit 0

###################
# PGAdmin (Postgresql)
###################

stop-pgadmin:
	docker stop te-pgadmin || exit 0
	docker rm -f te-pgadmin || exit 0

build-pgadmin:
	docker build \
		-f docker/dev/pgadmin.dockerfile \
		-t te/pgadmin:$(IMG_VERSION) .

run-pgadmin: stop-pgadmin
	docker run --network te \
		--name te-pgadmin -d \
		-p 15432:80 \
		te/pgadmin:$(IMG_VERSION)


########################
# DATABASE (Postgres)
########################

create-volume:
	docker volume create $(DB_VOLUME_NAME)_$(IMG_VERSION)

remove-volume:
	docker volume remove $(DB_VOLUME_NAME)_$(IMG_VERSION)

stop-database:
	docker stop te-postgres || exit 0
	docker rm -f te-postgres || exit 0

build-database:
	docker build \
		-f docker/dev/postgres.dockerfile \
		-t te/postgres:$(IMG_VERSION) .

run-database: stop-database create-volume
	docker run --network te \
		--name te-postgres -d \
		-v $(DB_VOLUME_NAME)_$(IMG_VERSION):/var/lib/postgresql/data \
		-p 5432:5432 \
		te/postgres:$(IMG_VERSION)

run-database-it: stop-database create-volume
	docker run --network te \
		--name te-postgres -it \
		-v $(DB_VOLUME_NAME)_$(IMG_VERSION):/var/lib/postgresql/data \
		-p 5432:5432 \
		te/postgres:$(IMG_VERSION)

################################################
# Seed for the Database (Needed on the Frontend)
################################################
# run-seed:
# 	docker exec -it caruana-sqlserver sqlcmd -i initialSqlTeste.sql -Usa -PCadastrob2ml!

###################
# EXTRA
###################

build: create-network build-database build-pgadmin
run: run-database run-pgadmin
stop: stop-database stop-pgadmin

## Show docker conteiners & Database volumes
show-conteiner:
	docker ps
show-volume:
	docker volume ls

## Database Logs
show-postgres-logs:
	docker logs -f te-postgres

## Docker prune all volumes (Like force reset)
do-prune-all:
	docker system prune -a -f --volumes


