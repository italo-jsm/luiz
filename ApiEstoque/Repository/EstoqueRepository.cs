﻿using ApiEstoque.Data;
using ApiEstoque.Repository.Interface;
using Infra.Models.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstoque.Repository
{
    public class EstoqueRepository : IEstoqueRepository
    {
        private readonly DataContext _context;

        public EstoqueRepository(DataContext context)
        {
            _context = context;
        }

        public IQueryable<Estoque> GetEstoques => _context.Set<Estoque>().AsQueryable();

        public Estoque EstoqueById(long id)
        {
            Estoque e = new Estoque();
            e = _context.Estoque.Include(x => x.Produto).Single(x => x.Id == id);
            return e;
        }

        public void InsertEstoque(Estoque e, string token)
        {
            Sessao s = _context.Sessao.Include(x => x.Usuario).Where(x => x.Chave == token).SingleOrDefault();
            if (e != null)
            {
                _context.Estoque.Add(e);
                _context.SaveChanges();
            }
        }

        public void UpdateEstoque(Estoque e)
        {
            Estoque eDb = _context.Estoque.Include(x => x.Produto).Single(x => x.Id == e.Id);
            if (e != null)
            {
                if (e.Produto != null) eDb.Produto = e.Produto;
                if (e.Quantidade > 0) eDb.Quantidade = e.Quantidade;
                if (e.Produto != null) eDb.Produto = e.Produto;

                _context.Estoque.Update(eDb);
                _context.SaveChanges();
            }
        }

        public void DeletaEstoque(int id)
        {
            Estoque eDb = _context.Estoque.Single(x => x.Id == id);
            if (id != 0)
            {
                _context.Estoque.Remove(eDb);
                _context.SaveChanges();
            }
        }
    }
}
