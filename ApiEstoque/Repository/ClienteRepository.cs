﻿using ApiEstoque.Data;
using ApiEstoque.Repository.Interface;
using Infra.Models.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstoque.Repository
{
    public class ClienteRepository : IClienteRepository
    {
        private readonly DataContext _context;

        public ClienteRepository(DataContext context)
        {
            _context = context;
        }

        public IQueryable<Cliente> GetListaClientes => _context.Set<Cliente>().AsQueryable();
        public IQueryable<Cliente> GetListaClientesPaginado => _context.Set<Cliente>().AsQueryable();

        public Cliente ClienteById(long id)
        {
            Cliente c = new Cliente();
            c = _context.Cliente.Include(x => x.Endereco).Single(x => x.Id == id);
            return c;
        }

        public void InsertCliente(Cliente c, string token)
        {
            // Sessao s = _context.Sessao.Include(x => x.Usuario).Where(x => x.Chave == token).SingleOrDefault();
            if (c != null)
            {
                c.DataCriacao = DateTimeOffset.UtcNow;
                // c.ResponsavelCriacao = s.Usuario.Id;

                _context.Cliente.Add(c);
                _context.SaveChanges();
            }
        }

        public void UpdateCliente(Cliente c)
        {
            Cliente cDb = _context.Cliente.Include(x => x.Endereco).Single(x => x.Id == c.Id);
            if (c != null)
            {
                if (c.Ativo != cDb.Ativo) cDb.Ativo = c.Ativo;
                if (c.DocumentoIdentificacao != null) cDb.DocumentoIdentificacao = c.DocumentoIdentificacao;
                if (c.RazaoSocial != null) cDb.RazaoSocial = c.RazaoSocial;
                if (c.Representante != null) cDb.Representante = c.Representante;
                if (c.Endereco != null) cDb.Endereco = _context.Endereco.Find(c.Endereco.Id);

                _context.Cliente.Update(cDb);
                _context.SaveChanges();
            }
        }

        public void DeleteCliente(Cliente c)
        {
            var cliente = _context.Cliente.Find(c);
            _context.Remove(cliente);
            _context.SaveChanges();
        }
    }
}
