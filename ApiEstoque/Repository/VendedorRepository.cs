﻿﻿using ApiEstoque.Data;
using ApiEstoque.Repository.Interface;
using Infra.Models.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstoque.Repository
{
    public class VendedorRepository : IVendedorRepository
    {

        private readonly DataContext _context;

        public VendedorRepository(DataContext context)
        {
            _context = context;
        }

        public IQueryable<Vendedor> GetVendedores => _context.Set<Vendedor>().AsQueryable();

        public void InsertVendedor(Vendedor v, string token)
        {
            // Sessao s = _context.Sessao.Include(x => x.Usuario).Where(x => x.Chave == token).SingleOrDefault();
            if(v != null)
            {
                v.DataCriacao = DateTimeOffset.UtcNow;
                // v.ResponsavelCriacao = s.Usuario.Id;

                _context.Vendedor.Add(v);
                _context.SaveChanges();
            }
        }

        public void UpdateVendedor(Vendedor v)
        {
            Vendedor vDb = _context.Vendedor.Where(x => x.Id == v.Id).SingleOrDefault();
            if(v != null)
            {
                if (v.NomeCompleto != vDb.NomeCompleto) vDb.NomeCompleto = v.NomeCompleto;
                if (v.Cpf != null) vDb.Cpf = v.Cpf;
                if (v.Departamento != null) vDb.Departamento = v.Departamento;

                _context.Vendedor.Update(vDb);
                _context.SaveChanges();
            }
        }

        public Vendedor VendedorById(long id)
        {
            Vendedor v = new Vendedor();
            v = _context.Vendedor.Single(x => x.Id == id);
            return v;
        }
    }
}
