﻿using ApiEstoque.Data;
using ApiEstoque.Repository.Interface;
using EmailUtil;
using Infra.Models.Database;
using Infra.Models.DTO;
using Infra.Models.Utils;
using Infra.Utils;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;

namespace ApiEstoque.Repository
{
    public class UsuarioRepository : IUsuarioRepository
    {
        private readonly DataContext _context;

        public UsuarioRepository(DataContext context)
        {
            _context = context;
        }

        public IQueryable<Usuario> GetListaUsuario => _context.Set<Usuario>().AsQueryable();

        public IQueryable<Usuario> GetListaUsuarioPaginado => _context.Set<Usuario>().AsQueryable();

        public void InsertUsuario(Usuario usuario, string token)
        {
            //Sessao s = _context.Sessao.Include(x => x.Usuario).Where(x => x.Chave == token).SingleOrDefault();
            if (usuario != null)
            {
                Usuario u = usuario;
                #region Gerando senha e Mandando para E-mail
                // string senha = new Random().Next(100000, 999999).ToString();
                // usuario.Senha = BCrypt.Net.BCrypt.HashPassword(senha);
                // SMTP smtp = new SMTP();
                // Email mail = new Email("anderson@b2ml.com.br", usuario.Email);
                // mail.SetHtmlTemplate(@"EmailsTemplates/email-inlined.html");
                // mail.SetSubject("Buspay - Conta Criada");
                // mail.ReplaceBody("{{preview}}", "Seu e-mail foi cadastrado na Buspay.");
                // mail.ReplaceBody("{{title}}", "Nova Conta");
                // mail.ReplaceBody("{{firstLine}}", "Bem vindo, " + usuario.NomeCompleto + "!");
                // mail.ReplaceBody("{{secondLine}}",
                //     "Seu acesso foi criado no sistema Buspay. Utilize seu email e a senha " + senha +
                //     " e faça seu primeiro login.");
                // smtp.Connect();
                // smtp.Send(mail.GetEMail());
                #endregion

                u.DataCriacao = DateTimeOffset.UtcNow;
                u.Perfil = _context.Perfil.Find(usuario.Perfil.Id);
                //usuario.ResponsavelCriacao = s.Usuario.Id;

                _context.Usuario.Add(u);
                _context.SaveChanges();
            }
        }

        public void UpdateUsuario(Usuario usuario)
        {
            Usuario uDb = _context.Usuario.Include(x => x.Perfil).Where(x => x.Id == usuario.Id).SingleOrDefault();
            if (usuario != null)
            {
                if (usuario.NomeCompleto != uDb.NomeCompleto) uDb.NomeCompleto = usuario.NomeCompleto;
                if (usuario.Cpf != null) uDb.Cpf = usuario.Cpf;
                if (usuario.Email != null) uDb.Email = usuario.Email;
                if (usuario.Senha != null) uDb.Senha = usuario.Senha;
                if (usuario.Perfil != null) uDb.Perfil = usuario.Perfil;

                _context.Usuario.Update(uDb);
                _context.SaveChanges();
            }

        }

        public Usuario UsuarioById(long id)
        {
            Usuario u = new Usuario();
            u = _context.Usuario.Include(x => x.Perfil).Single(x => x.Id == id);
            return u;
        }

        public LoginDTO PostUsuarioLoginByEmail(Usuario u)
        {
            LoginDTO login = new LoginDTO();
            Usuario usrDB = _context.Usuario.Include(x => x.Perfil).Single(x => x.Email == u.Email);
            if (usrDB == null)
            {
                //return Resposta.NoContent("Usuário", "m");
                login.Mensagem = "O Usuário não cadastrado.";
                return login;
            }
            login =  PostUsuarioLogin(usrDB, u);
            if (login == null)
            {
                login = new LoginDTO();
                login.Mensagem = "A Senha está incorreta.";
                return login;
            }
            return login;
        }

        public LoginDTO PostUsuarioLogin(Usuario usuario, Usuario usuarioDTO)
        {
            string token = null;
            Console.WriteLine("Usuario: {0}", JsonConvert.SerializeObject(usuarioDTO, Formatting.Indented));
            if (BCrypt.Net.BCrypt.Verify(usuarioDTO.Senha, usuario.Senha))
            {
                token = Guid.NewGuid().ToString("N");
                Sessao s = new Sessao
                {
                    Ativo = true,
                    Chave = token,
                    Data = DateTimeOffset.UtcNow,
                    Usuario = usuario,
                    Validade = DateTimeOffset.MaxValue
                };

                _context.Entry(usuario).Reference(p => p.Perfil).Load();

                LoginDTO loginAutenticado = new LoginDTO
                {
                    Id = usuario.Id,
                    Nome = usuario.NomeCompleto,
                    Perfil = usuario.Perfil,
                    Token = token,
                };

                _context.Sessao.Add(s);
                _context.SaveChanges();

                return loginAutenticado;
            }
            else
            {
                return null;
            }
        }


        public Boolean GetResetSenha(string email)
        {
            //Console.WriteLine("\n##Email: {0}\n", email);
            Usuario usrDB = _context.Usuario.Include(x => x.Perfil).SingleOrDefault(x => x.Email.Equals(email));
            if (usrDB == null)
                return false;

            string senha = new Random().Next(100000, 999999).ToString();
            Console.WriteLine("Senha: {0}", senha);
            usrDB.Senha = BCrypt.Net.BCrypt.HashPassword(senha);

            SMTP smtp = new SMTP();
            Email mail = new Email("luuizgs123@gmail.com", email);
            mail.SetHtmlTemplate(@"EmailsTemplates/email-inlined.html");
            mail.SetSubject("MPX Stash - Reset Senha");
            mail.ReplaceBody("{{preview}}", "Sua senha do MPX Stash foi resetada.");
            mail.ReplaceBody("{{title}}", "Reset Senha");
            mail.ReplaceBody("{{firstLine}}", "Olá, " + usrDB.NomeCompleto + "!");
            mail.ReplaceBody("{{secondLine}}",
                "Sua nova senha é " + senha + ". Faça seu login e troque por uma senha forte.");
            smtp.Connect();
            smtp.Send(mail.GetEMail());

            _context.Usuario.Update(usrDB);

            try
            {
                _context.SaveChanges();
            }
            catch (Exception e)
            {
                System.Console.WriteLine("\nException: {0} {1}\n", e.Message, e.StackTrace);
                return false;

            }

            return true;
        }

        public Resposta Logout(string token, bool todos)
        {

            Sessao s = _context.Sessao.Where(x => x.Chave == token).SingleOrDefault();
            if (s == null)
                return Resposta.NoContent("Usuário", "m");

            if (todos)
            {
                List<Sessao> sessoes = _context.Sessao.Where(x => x.Ativo && x.Usuario.Id == s.Usuario.Id).ToList();
                foreach (Sessao aux in sessoes)
                {
                    aux.Ativo = false;
                    aux.Validade = DateTimeOffset.UtcNow;
                    _context.Update(aux);
                }
            }
            else
            {
                s.Ativo = false;
                s.Validade = DateTimeOffset.UtcNow;
                _context.Update(s);
            }

            try
            {
                _context.SaveChanges();
            }
            catch
            {
                return Resposta.BadRequest();
            }

            return new Resposta
            {
                Status = HttpStatusCode.OK,
                Descricao = "Logout realizado com sucesso"
            };
        }

        public Resposta CheckLogin(string token)
        {

            try
            {
                Sessao s = _context.Sessao.Where(x => x.Chave == token).SingleOrDefault();

                if (s == null)
                {
                    return Resposta.Forbidden();
                }

                return Resposta.Ok();

            }
            catch (System.Exception)
            {
                return Resposta.Forbidden();
            }
        }
    }
}
