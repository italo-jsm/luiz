﻿using ApiEstoque.Data;
using ApiEstoque.Repository.Interface;
using Infra.Models.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstoque.Repository
{
    public class PerfilRepository : IPerfilRepository
    {

        private readonly DataContext _context;

        public PerfilRepository(DataContext context)
        {
            _context = context;
        }
        public IQueryable<Perfil> GetTodosPerfis => _context.Set<Perfil>().AsQueryable();

        public void InsertPerfil(Perfil p, string token)
        {
            try
            {
                // Sessao s = _context.Sessao.Include(x => x.Usuario).Where(x => x.Chave == token).SingleOrDefault();
                if (p != null)
                {
                    _context.Perfil.Add(p);
                    try
                    {
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("\nException: {0} \n{1}", e.Message, e.StackTrace);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\nSessão Exception: {0} \n{1}", e.Message, e.StackTrace);
            }
        }

        public Perfil PerfilById(long id)
        {
            try
            {
                Perfil p = _context.Perfil.Single(x => x.Id == id);
                return p;
            }
            catch (Exception e)
            {
                Console.WriteLine("\nException: {0} \n{1}", e.Message, e.StackTrace);
                return null;
            }
        }

        public void UpdatePerfil(Perfil p)
        {
            try
            {
                Perfil pDb = _context.Perfil.Single(c => c.Id == p.Id);
                if (p != null)
                {
                    if (p.Descricao != null) pDb.Descricao = p.Descricao;
                    if (p.Nome != null) pDb.Nome = p.Nome;
                }
                _context.Perfil.Add(pDb);
                try
                {
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine("\nException: {0} \n{1}", e.Message, e.StackTrace);
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\nException: {0} \n{1}", e.Message, e.StackTrace);
            }
        }
    }
}
