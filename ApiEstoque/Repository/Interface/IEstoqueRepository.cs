﻿using Infra.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstoque.Repository.Interface
{
    public interface IEstoqueRepository
    {

        IQueryable<Estoque> GetEstoques { get; }

        Estoque EstoqueById(long id);

        void InsertEstoque(Estoque e, string token);

        void UpdateEstoque(Estoque e);
        void DeletaEstoque(int id);
    }
}
