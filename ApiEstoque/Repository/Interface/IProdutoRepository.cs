using System.Collections.Generic;
using System.Linq;
using Infra.Models.Database;

namespace ApiEstoque.Repository.Interface
{
    public interface IProdutoRepository
    {
        IQueryable<Produto> GetProdutos { get; }
        List<Produto> GetTodosProdutos();
        Produto ProdutoById(long id);
        void InsertProduto(Produto produto, string token);
        void UpdateProduto(Produto produto);
    }
}