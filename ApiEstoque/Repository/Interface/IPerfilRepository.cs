﻿using Infra.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstoque.Repository.Interface
{
    public interface IPerfilRepository
    {
        IQueryable<Perfil> GetTodosPerfis { get; }

        Perfil PerfilById(long id);

        void InsertPerfil(Perfil p, string token);

        void UpdatePerfil(Perfil p);
    }
}
