﻿using Infra.Models.Database;
using Infra.Models.DTO;
using Infra.Models.Utils;
using Infra.Utils;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstoque.Repository.Interface
{
    public interface IUsuarioRepository
    {
        IQueryable<Usuario> GetListaUsuario { get; }
        IQueryable<Usuario> GetListaUsuarioPaginado { get; }
        Usuario UsuarioById(long id);
        void InsertUsuario(Usuario usuario, string token);
        void UpdateUsuario(Usuario usuario);
        LoginDTO PostUsuarioLoginByEmail(Usuario u);
        LoginDTO PostUsuarioLogin(Usuario usuario, Usuario usuarioDTO);
        Boolean GetResetSenha(string email);
        Resposta Logout(string token, bool todos);
        Resposta CheckLogin(string token);

    }
}
