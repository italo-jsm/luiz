﻿using Infra.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstoque.Repository.Interface
{
    public interface IVendedorRepository
    {
        IQueryable<Vendedor> GetVendedores { get; }
        Vendedor VendedorById(long id);
        void InsertVendedor(Vendedor v, string token);
        void UpdateVendedor(Vendedor v);
    }
}
