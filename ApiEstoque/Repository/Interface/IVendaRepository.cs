﻿using Infra.Models.Database;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstoque.Repository.Interface
{
    public interface IVendaRepository
    {
        IQueryable<Venda> GetVendas { get; }
        Venda VendaById(long id);
        void InsertVenda(Venda v, string token);
        void UpdateVenda(Venda v);
    }
}
