﻿using Infra.Models.Database;
using System.Linq;

namespace ApiEstoque.Repository.Interface
{
    public interface IClienteRepository
    {
        IQueryable<Cliente> GetListaClientes { get; }
        IQueryable<Cliente> GetListaClientesPaginado { get; }
        Cliente ClienteById(long id);
        void InsertCliente(Cliente c, string token);
        void UpdateCliente(Cliente c);
    }
}
