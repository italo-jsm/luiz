﻿using ApiEstoque.Data;
using ApiEstoque.Repository.Interface;
using Infra.Models.Database;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace ApiEstoque.Repository
{
    public class VendaRepository : IVendaRepository
    {
        private readonly DataContext _context;

        public VendaRepository(DataContext context)
        {
            _context = context;
        }

        public IQueryable<Venda> GetVendas => _context.Set<Venda>().AsQueryable();

        public void InsertVenda(Venda v, string token)
        {
            try
            {
                Sessao s = _context.Sessao.Include(x => x.Usuario).Where(c => c.Chave == token).SingleOrDefault();
                if (v != null)
                {
                    _context.Venda.Add(v);
                    try
                    {
                        _context.SaveChanges();
                    }
                    catch (Exception e)
                    {
                        Console.WriteLine("\nDB Exception: {0}\n{1}", e.Message, e.StackTrace);
                    }
                }
            }
            catch (Exception e)
            {
                Console.WriteLine("\nSessão Exception: {0}\n{1}", e.Message, e.StackTrace);
            }
        }

        public void UpdateVenda(Venda v)
        {
            Venda vDb = _context.Venda.Single(x => x.Id == v.Id);
            if(v != null)
            {
                if (v.Produto != null) vDb.Produto = v.Produto;
                if (v.Quantidade != vDb.Quantidade) vDb.Quantidade = v.Quantidade;
                if (v.ValorTotal != vDb.ValorTotal) vDb.ValorTotal = v.ValorTotal;
                if (v.Vendedor != vDb.Vendedor) vDb.Vendedor = v.Vendedor;

                _context.Venda.Update(vDb);
                try
                {
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    Console.WriteLine("\nException: {0} \n{1}", e.Message, e.StackTrace);
                }
            }
        }

        public Venda VendaById(long id)
        {
            try
            {
                Venda v = _context.Venda.Single(x => x.Id == id);
                return v;
            }
            catch (Exception e)
            {
                Console.WriteLine("\nException: {0} \n{1}", e.Message, e.StackTrace);
                return null;
            }
        }
    }
}
