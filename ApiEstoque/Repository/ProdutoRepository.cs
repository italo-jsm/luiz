using System;
using System.Collections.Generic;
using System.Linq;
using ApiEstoque.Data;
using ApiEstoque.Repository.Interface;
using Infra.Models.Database;
using Microsoft.EntityFrameworkCore;

namespace ApiEstoque.Repository
{
    public class ProdutoRepository : IProdutoRepository
    {
        private readonly DataContext _context;

        public ProdutoRepository(DataContext context)
        {
            _context = context;
        }

        public IQueryable<Produto> GetProdutos => _context.Set<Produto>().AsQueryable();

        public Produto ProdutoById(long id)
        {
            Produto p = new Produto();
            p = _context.Produto.Find(id);
            return p;
        }

        public List<Produto> GetTodosProdutos()
        {
            List<Produto> todos = new List<Produto>();
            todos = _context.Produto.ToList();
            return todos;
        }

        public void InsertProduto(Produto produto, string token)
        {
            // Sessao s = _context.Sessao.Include(x => x.Usuario).Where(x => x.Chave.Equals(token)).SingleOrDefault();
            if (produto != null)
            {
                produto.DataCriacao = DateTimeOffset.UtcNow;
                // produto.ResponsavelCriacao = s.Usuario.Id;

                _context.Produto.Add(produto);
                try
                {
                    _context.SaveChanges();
                }
                catch (Exception e)
                {
                    System.Console.WriteLine("Exception: {0} {1}", e.Message, e.StackTrace);
                }
            }
        }

        public void UpdateProduto(Produto produto)
        {
            Produto pDb = _context.Produto.Where(x => x.Id == produto.Id).SingleOrDefault();
            if (produto != null)
            {
                if (produto.Ativo != pDb.Ativo) pDb.Ativo = produto.Ativo;
                if (produto.Descricao != null) pDb.Descricao = produto.Descricao;
                if (produto.Fabricante != null) pDb.Fabricante = produto.Fabricante;
                if (produto.Nome != null) pDb.Nome = produto.Nome;
                if (produto.Valor > 0) pDb.Valor = produto.Valor;

                _context.Produto.Update(pDb);
                try
                {
                    _context.SaveChanges();
                }
                catch (System.Exception e)
                {
                    System.Console.WriteLine("Exception: {0} {1}", e.Message, e.StackTrace);
                }
            }
        }
    }
}