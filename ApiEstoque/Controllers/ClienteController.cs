﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiEstoque.Repository.Interface;
using Infra.Models.Database;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Infra.Extensions;
using Infra.Utils.Filters;
using Infra.Utils.Ordenacao.Model;
using Infra.Utils.Paginacao.Model;
using Infra.Utils.Ordenacao;
using Infra.Utils.Paginacao;

namespace ApiEstoque.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClienteController : ControllerBase
    {
        private readonly IClienteRepository _clienteRepository;

        public ClienteController(IClienteRepository repository)
        {
            _clienteRepository = repository;
        }

        // GET: api/Cliente
        [HttpGet]
        public IActionResult GetListaClientes([FromQuery] ClienteFilters filtro,
                                              [FromQuery] Ordem ordem,
                                              [FromQuery] ObjetoPaginacao paginacao)
        {
            var lista = _clienteRepository.GetListaClientes
                            .ClienteFiltros(filtro)
                            .AplicaOrdenacaoCliente(ordem);

            var listaRetorno = lista.ToList();
            if (listaRetorno.Count == 0)
            {
                // return NotFound("Nenhum vendedor atende os requisitos!");
                return NoContent();
            }

            return Ok(listaRetorno);
        }

        // GET: api/Cliente/paginado
        [HttpGet("paginado")]
        public IActionResult GetListaClientesPaginado([FromQuery] ClienteFilters filtro,
                                                      [FromQuery] Ordem ordem,
                                                      [FromQuery] ObjetoPaginacao paginacao)
        {
            var lista = _clienteRepository.GetListaClientesPaginado
                            .ClienteFiltros(filtro)
                            .AplicaOrdenacaoCliente(ordem);

            var listaPaginada = Paginacao<Cliente>.From(paginacao, lista);

            if (listaPaginada.ListaItens.Count == 0) return NoContent();

            return Ok(listaPaginada);
        }

        // GET: api/Cliente/5
        [HttpGet("{id}")]
        public IActionResult Get(long id)
        {
            string token = HttpContext.Request.Headers["AccessToken"];
            //TODO: Faz uma função na infra (Validate) pra validar as permissões do usuario para acessar esse metodo
            var cliente = _clienteRepository.ClienteById(id);
            if (cliente != null)
                return Ok(cliente);

            return NotFound("Cliente não encontrado!");
        }

        // POST: api/Cliente
        [HttpPost]
        public IActionResult Post([FromBody] Cliente cliente)
        {            
            if (ModelState.IsValid)
            {
                string token = HttpContext.Request.Headers["AccessToken"];

                _clienteRepository.InsertCliente(cliente, token);
                return Ok(new { Id = cliente.Id });
            }
            return BadRequest();
        }

        // PUT: api/Cliente
        [HttpPut]
        public IActionResult Put([FromBody] Cliente cliente)
        {
            if (ModelState.IsValid)
            {
                _clienteRepository.UpdateCliente(cliente);
                return Ok(JsonConvert.SerializeObject("Cliente atualizado com sucesso."));
            }
            return BadRequest(JsonConvert.SerializeObject("Erro ao atualizar o Cliente."));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            string mensagem = "Method not allowed. It Requires Administrator permission.";
            return Forbid(mensagem);
        }
    }
}
