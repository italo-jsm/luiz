using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiEstoque.Repository.Interface;
using Infra.Extensions;
using Infra.Models.Database;
using Infra.Utils.Filters;
using Infra.Utils.Ordenacao;
using Infra.Utils.Ordenacao.Model;
using Infra.Utils.Paginacao;
using Infra.Utils.Paginacao.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ApiEstoque.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendaController : ControllerBase
    {

        private readonly IVendaRepository _VendaRepository;

        public VendaController(IVendaRepository repository)
        {
            _VendaRepository = repository;
        }

        // GET: api/Venda
        [HttpGet]
        public IActionResult Get([FromQuery] VendaFilters filtro,
                                 [FromQuery] Ordem ordem,
                                 [FromQuery] ObjetoPaginacao paginacao)
        {
            var lista = _VendaRepository.GetVendas
                            .VendaFiltro(filtro)
                            .AplicaOrdenacaoVenda(ordem);

            var listaPaginada = Paginacao<Venda>.From(paginacao, lista);

            if (listaPaginada.ListaItens.Count == 0) return NoContent();

            return Ok(listaPaginada);
        }

        // GET: api/Venda/5
        [HttpGet("{id}")]
        public IActionResult Get(long id)
        {
            string token = HttpContext.Request.Headers["AccessToken"];

            var Venda = _VendaRepository.VendaById(id);
            if (Venda != null)
                return Ok(Venda);

            return NotFound("Venda não encontrado");
        }

        // POST: api/Venda
        [HttpPost]
        public IActionResult Post([FromBody] Venda Venda)
        {
            if(ModelState.IsValid)
            {
                string token = HttpContext.Request.Headers["AcessToken"];

                _VendaRepository.InsertVenda(Venda, token);
                return Ok(new { Id = Venda.Id });
            }
            return BadRequest();
        }

        // PUT: api/Venda/5
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] Venda Venda)
        {
            if(ModelState.IsValid)
            {
                _VendaRepository.UpdateVenda(Venda);
                return Ok(JsonConvert.SerializeObject("Venda atualizado com sucesso"));
            }
            return BadRequest(JsonConvert.SerializeObject("Erro ao atualizar Venda"));
        }
    }
}
