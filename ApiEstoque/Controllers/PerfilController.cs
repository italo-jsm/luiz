using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiEstoque.Repository.Interface;
using Infra.Models.Database;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using Infra.Extensions;
using Infra.Utils.Filters;
using Infra.Utils.Ordenacao.Model;
using Infra.Utils.Paginacao.Model;
using Infra.Utils.Ordenacao;
using Infra.Utils.Paginacao;

namespace ApiEstoque.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class PerfilController : ControllerBase
    {
        private readonly IPerfilRepository _repository;

        public PerfilController(IPerfilRepository repository)
        {
            _repository = repository;
        }

        // GET: api/Perfil
        [HttpGet]
        public IActionResult GetListaPerfis([FromQuery] PerfilFilters filtro,
                                              [FromQuery] Ordem ordem,
                                              [FromQuery] ObjetoPaginacao paginacao)
        {
            var lista = _repository.GetTodosPerfis
                            .PerfilFiltro(filtro)
                            .AplicaOrdenacaoPerfil(ordem);

            var listaRetorno = lista.ToList();
            if (listaRetorno.Count == 0)
            {
                // return NotFound("Nenhum vendedor atende os requisitos!");
                return NoContent();
            }

            return Ok(listaRetorno);
        }

        // GET: api/Perfil/paginado
        [HttpGet("paginado")]
        public IActionResult GetListaPerfisPaginado([FromQuery] PerfilFilters filtro,
                                                     [FromQuery] Ordem ordem,
                                                     [FromQuery] ObjetoPaginacao paginacao)
        {
            var lista = _repository.GetTodosPerfis
                            .PerfilFiltro(filtro)
                            .AplicaOrdenacaoPerfil(ordem);

            var listaPaginada = Paginacao<Perfil>.From(paginacao, lista);

            if (listaPaginada.ListaItens.Count == 0) return NoContent();

            return Ok(listaPaginada);
        }

        // GET: api/Perfil/5
        [HttpGet("{id}")]
        public IActionResult Get(long id)
        {
            var Perfil = _repository.PerfilById(id);
            if (Perfil != null)
                return Ok(Perfil);

            return NotFound(new { mensagem = "Perfil não encontrado!" });
        }

        // POST: api/Perfil
        [HttpPost]
        public IActionResult Post([FromBody] Perfil Perfil)
        {            
            if (ModelState.IsValid)
            {
                string token = HttpContext.Request.Headers["AccessToken"];

                _repository.InsertPerfil(Perfil, token);
                return Ok(new { Id = Perfil.Id });
            }
            return BadRequest();
        }

        // PUT: api/Perfil
        [HttpPut]
        public IActionResult Put([FromBody] Perfil Perfil)
        {
            if (ModelState.IsValid)
            {
                _repository.UpdatePerfil(Perfil);
                return Ok(new { mensagem = "Perfil atualizado com sucesso." });
            }
            return BadRequest(new { mensagem = "Erro ao atualizar o Perfil." });
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete([FromRoute] int id)
        {
            string mensagem = "Method not allowed. It Requires Administrator permission.";
            return Forbid(mensagem);
        }
    }
}
