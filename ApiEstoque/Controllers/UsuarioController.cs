﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiEstoque.Repository.Interface;
using Infra.Extensions;
using Infra.Models.Database;
using Infra.Models.DTO;
using Infra.Models.Utils;
using Infra.Utils.Filters;
using Infra.Utils.Ordenacao;
using Infra.Utils.Ordenacao.Model;
using Infra.Utils.Paginacao;
using Infra.Utils.Paginacao.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ApiEstoque.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class UsuarioController : ControllerBase
    {

        private readonly IUsuarioRepository _usuarioRepository;

        public UsuarioController(IUsuarioRepository repository)
        {
            _usuarioRepository = repository;
        }

        // GET: api/Usuario
        [HttpGet]
        public IActionResult GetListaUsuario([FromQuery] UsuarioFilters filtro,
                                             [FromQuery] Ordem ordem,
                                             [FromQuery] ObjetoPaginacao paginacao)
        {
            var lista = _usuarioRepository.GetListaUsuario
                            .UsuarioFiltro(filtro)
                            .AplicaOrdenacaoUsuario(ordem);

            //var listaPaginada = Paginacao<Usuario>.From(paginacao, lista);

            var listaRetorno = lista.ToList();
            if (listaRetorno.Count == 0)
            {
                // return NotFound("Nenhum vendedor atende os requisitos!");
                return NoContent();
            }


            return Ok(listaRetorno);
        }

        [HttpGet("paginado")]
        public IActionResult GetListaUsuarioPaginado([FromQuery] UsuarioFilters filtro,
                                                     [FromQuery] Ordem ordem,
                                                     [FromQuery] ObjetoPaginacao paginacao)
        {
            var lista = _usuarioRepository.GetListaUsuarioPaginado
                            .UsuarioFiltro(filtro)
                            .AplicaOrdenacaoUsuario(ordem);

            var listaPaginada = Paginacao<Usuario>.From(paginacao, lista);

            if (listaPaginada.ListaItens.Count == 0) return NoContent();

            return Ok(listaPaginada);
        }

        // GET: api/Usuario/5
        [HttpGet("{id}")]
        public IActionResult GetUsuarioById(long id)
        {
            // string token = HttpContext.Request.Headers["AccessToken"];

            var usuario = _usuarioRepository.UsuarioById(id);
            if (usuario != null)
                return Ok(usuario);

            return NotFound(new { mensagem = "Usuario não encontrado" });
        }

        // POST: api/Usuario
        [HttpPost]
        public IActionResult Post([FromBody] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                string token = HttpContext.Request.Headers["AcessToken"];

                _usuarioRepository.InsertUsuario(usuario, token);
                return CreatedAtAction("GetUsuarioById", new { Id = usuario.Id }, usuario);
                //return Ok(new { Id = usuario.Id });
            }
            return BadRequest();
        }

        // PUT: api/Usuario/5
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                _usuarioRepository.UpdateUsuario(usuario);
                return Ok(new { mensagem = "Usuario atualizado com sucesso" });
            }
            return BadRequest(new { mensagem = "Erro ao atualizar usuario" });
        }

        // POST: api/Usuario/Login/
        [HttpPost("Login")]
        public IActionResult Login([FromBody] Usuario u)
        {
            LoginDTO login = _usuarioRepository.PostUsuarioLoginByEmail(u);
            if (login == null)
            {
                return BadRequest("Login não autenticado");
            }
            return Ok(login);
        }

        // Get: api/Usuario/ResetSenha/email
       [HttpGet("ResetSenha/{email}")]
        public IActionResult ResetSenha([FromRoute] string email)
        {
            if (_usuarioRepository.GetResetSenha(email))
                return Ok("E-mail enviado para o e-mail.");
            else
                return BadRequest();
        }

        // GET: api/Usuario/CheckLogin/
        [HttpGet("CheckLogin")]
        public Resposta CheckLogin()
        {
            string token = HttpContext.Request.Headers["AccessToken"];
            return _usuarioRepository.CheckLogin(token);
        }

        // Get: api/Usuario/Logout/{todos}
        [HttpGet("Logout/{todos}")]
        public Resposta Logout([FromRoute] bool todos)
        {
            string token = HttpContext.Request.Headers["AccessToken"];
            return _usuarioRepository.Logout(token, todos);
        }
    }
}
