using ApiEstoque.Repository.Interface;
using Infra.Extensions;
using Infra.Models.Database;
using Infra.Utils.Filters;
using Infra.Utils.Ordenacao;
using Infra.Utils.Ordenacao.Model;
using Infra.Utils.Paginacao;
using Infra.Utils.Paginacao.Model;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ApiEstoque.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ProdutoController : ControllerBase
    {
        private readonly IProdutoRepository _produtoRepository;

        public ProdutoController(IProdutoRepository repository)
        {
            _produtoRepository = repository;
        }

        // GET: api/Produto
        [HttpGet]
        public IActionResult Get([FromQuery] ProdutoFilters filtro,
                                 [FromQuery] Ordem ordem,
                                 [FromQuery] ObjetoPaginacao paginacao)
        {
            var lista = _produtoRepository.GetProdutos
                                    .ProdutoFiltro(filtro)
                                    .AplicaOrdenacaoProduto(ordem);

            var listaPaginada = Paginacao<Produto>.From(paginacao, lista);

            if (listaPaginada.ListaItens.Count == 0) return NoContent();

            return Ok(listaPaginada);
        }

        // GET: api/Produto/Todos
        [HttpGet("Todos")]
        public IActionResult GetTodos()
        {
            var lista = _produtoRepository.GetTodosProdutos();

            if (lista.Count == 0) return NoContent();

            return Ok(lista);
        }

        //GET: api/produto/5
        [HttpGet("{id}")]
        public IActionResult GetProduto(long id)
        {
            string token = HttpContext.Request.Headers["AccessToken"];

            var produto = _produtoRepository.ProdutoById(id);
            if (produto != null)
                return Ok(produto);

            return NotFound("Cliente não encontrado");
        }

        //POST: api/Produto
        [HttpPost]
        public IActionResult Post([FromBody] Produto produto)
        {
            if(ModelState.IsValid)
            {
                string token = HttpContext.Request.Headers["AccessToken"];

                _produtoRepository.InsertProduto(produto, token);
                return Ok(new { Id = produto.Id });
            }
            return BadRequest();
        }

        //PUT: api/Produto
        [HttpPut]
        public IActionResult Put([FromBody] Produto produto)
        {
            if(ModelState.IsValid)
            {
                _produtoRepository.UpdateProduto(produto);
                return Ok(JsonConvert.SerializeObject("Atualizado com Sucesso"));
            }
            return BadRequest(JsonConvert.SerializeObject("Erro ao Atualizar"));
        }
      
    }
}