﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiEstoque.Repository.Interface;
using Infra.Extensions;
using Infra.Models.Database;
using Infra.Utils.Filters;
using Infra.Utils.Ordenacao;
using Infra.Utils.Ordenacao.Model;
using Infra.Utils.Paginacao;
using Infra.Utils.Paginacao.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ApiEstoque.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class EstoqueController : ControllerBase
    {
        private readonly IEstoqueRepository _estoqueRepository;

        public EstoqueController(IEstoqueRepository repository)
        {
            _estoqueRepository = repository;
        }

        // GET: api/Estoque
        [HttpGet]
        public IActionResult Get([FromQuery] EstoqueFilters filtro,
                                 [FromQuery] Ordem ordem,
                                 [FromQuery] ObjetoPaginacao paginacao)
        {
            var lista = _estoqueRepository.GetEstoques.
                                                EstoqueFiltro(filtro).
                                                AplicaOrdenacaoEstoque(ordem);

            var listaPaginada = Paginacao<Estoque>.From(paginacao, lista);

            if (listaPaginada.ListaItens.Count == 0) return NoContent();

            return Ok(lista);
        }

        // GET: api/Estoque/5
        [HttpGet("{id}")]
        public IActionResult GetEstoque(int id)
        {
            string token = HttpContext.Request.Headers["AccessToekn"];

            var estoque = _estoqueRepository.EstoqueById(id);
            if (estoque != null)
                return Ok(estoque);

            return NotFound("Estoque Não Encontrado");
        }

        // POST: api/Estoque
        [HttpPost]
        public IActionResult Post([FromBody] Estoque estoque)
        {
            if(ModelState.IsValid)
            {
                string token = HttpContext.Response.Headers["AccessToken"];

                _estoqueRepository.InsertEstoque(estoque, token);
                return Ok(new { Id = estoque.Id });
            }
            return BadRequest();
        }

        // PUT: api/Estoque/5
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] Estoque estoque)
        {
            if(ModelState.IsValid)
            {
                _estoqueRepository.UpdateEstoque(estoque);
                return Ok(JsonConvert.SerializeObject("Atuzalizado com Sucesso!"));
            }
            return BadRequest(JsonConvert.SerializeObject("Erro ao Atualizar"));
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public IActionResult Delete(int id)
        {
            if(id != 0)
            {
                _estoqueRepository.DeletaEstoque(id);
                return Ok(JsonConvert.SerializeObject("Remocido com Sucesso!"));
            }
            return BadRequest(JsonConvert.SerializeObject("Erro ao Remover"));
        }
    }
}
