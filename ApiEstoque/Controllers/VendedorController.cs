using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ApiEstoque.Repository.Interface;
using Infra.Extensions;
using Infra.Models.Database;
using Infra.Utils.Filters;
using Infra.Utils.Ordenacao;
using Infra.Utils.Ordenacao.Model;
using Infra.Utils.Paginacao;
using Infra.Utils.Paginacao.Model;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace ApiEstoque.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class VendedorController : ControllerBase
    {

        private readonly IVendedorRepository _VendedorRepository;

        public VendedorController(IVendedorRepository repository)
        {
            _VendedorRepository = repository;
        }

        // GET: api/Vendedor
        [HttpGet]
        public IActionResult Get([FromQuery] VendedorFilters filtro,
                                 [FromQuery] Ordem ordem,
                                 [FromQuery] ObjetoPaginacao paginacao)
        {
            var lista = _VendedorRepository.GetVendedores
                            .VendedorFiltro(filtro)
                            .AplicaOrdenacaoVendedor(ordem);

            var listaPaginada = Paginacao<Vendedor>.From(paginacao, lista);

            if (listaPaginada.ListaItens.Count == 0) return NoContent();

            return Ok(listaPaginada);
        }

        // GET: api/Vendedor/5
        [HttpGet("{id}")]
        public IActionResult Get(long id)
        {
            string token = HttpContext.Request.Headers["AccessToken"];

            var Vendedor = _VendedorRepository.VendedorById(id);
            if (Vendedor != null)
                return Ok(Vendedor);

            return NotFound("Vendedor não encontrado");
        }

        // POST: api/Vendedor
        [HttpPost]
        public IActionResult Post([FromBody] Vendedor Vendedor)
        {
            if(ModelState.IsValid)
            {
                string token = HttpContext.Request.Headers["AcessToken"];

                _VendedorRepository.InsertVendedor(Vendedor, token);
                return Ok(new { Id = Vendedor.Id });
            }
            return BadRequest();
        }

        // PUT: api/Vendedor/5
        [HttpPut("{id}")]
        public IActionResult Put([FromBody] Vendedor Vendedor)
        {
            if(ModelState.IsValid)
            {
                _VendedorRepository.UpdateVendedor(Vendedor);
                return Ok(JsonConvert.SerializeObject("Vendedor atualizado com sucesso"));
            }
            return BadRequest(JsonConvert.SerializeObject("Erro ao atualizar Vendedor"));
        }
    }
}
