﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace ApiEstoque.Migrations
{
    public partial class ChangeOnVendedor : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Venda_Usuario_VendedorId",
                table: "Venda");

            migrationBuilder.DropColumn(
                name: "Discriminator",
                table: "Usuario");

            migrationBuilder.DropColumn(
                name: "Codigo",
                table: "Usuario");

            migrationBuilder.DropColumn(
                name: "Departamento",
                table: "Usuario");

            migrationBuilder.CreateTable(
                name: "Vendedor",
                columns: table => new
                {
                    Id = table.Column<long>(nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.SerialColumn),
                    DataCriacao = table.Column<DateTimeOffset>(nullable: false),
                    DataModificacao = table.Column<DateTimeOffset>(nullable: false),
                    ResponsavelCriacao = table.Column<long>(nullable: false),
                    ResponsavelModificacao = table.Column<long>(nullable: false),
                    Ativo = table.Column<bool>(nullable: false),
                    NomeCompleto = table.Column<string>(nullable: true),
                    Cpf = table.Column<string>(nullable: true),
                    Codigo = table.Column<long>(nullable: false),
                    Departamento = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Vendedor", x => x.Id);
                });

            migrationBuilder.AddForeignKey(
                name: "FK_Venda_Vendedor_VendedorId",
                table: "Venda",
                column: "VendedorId",
                principalTable: "Vendedor",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Venda_Vendedor_VendedorId",
                table: "Venda");

            migrationBuilder.DropTable(
                name: "Vendedor");

            migrationBuilder.AddColumn<string>(
                name: "Discriminator",
                table: "Usuario",
                nullable: false,
                defaultValue: "");

            migrationBuilder.AddColumn<long>(
                name: "Codigo",
                table: "Usuario",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Departamento",
                table: "Usuario",
                nullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Venda_Usuario_VendedorId",
                table: "Venda",
                column: "VendedorId",
                principalTable: "Usuario",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
