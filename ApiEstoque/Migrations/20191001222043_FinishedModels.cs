﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace ApiEstoque.Migrations
{
    public partial class FinishedModels : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PerfilPermissao_MyProperty_PerfilId",
                table: "PerfilPermissao");

            migrationBuilder.DropForeignKey(
                name: "FK_Usuario_MyProperty_PerfilId",
                table: "Usuario");

            migrationBuilder.DropPrimaryKey(
                name: "PK_MyProperty",
                table: "MyProperty");

            migrationBuilder.DropColumn(
                name: "SenhaConfere",
                table: "Usuario");

            migrationBuilder.DropColumn(
                name: "SenhaNova",
                table: "Usuario");

            migrationBuilder.RenameTable(
                name: "MyProperty",
                newName: "Perfil");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Perfil",
                table: "Perfil",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PerfilPermissao_Perfil_PerfilId",
                table: "PerfilPermissao",
                column: "PerfilId",
                principalTable: "Perfil",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Usuario_Perfil_PerfilId",
                table: "Usuario",
                column: "PerfilId",
                principalTable: "Perfil",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_PerfilPermissao_Perfil_PerfilId",
                table: "PerfilPermissao");

            migrationBuilder.DropForeignKey(
                name: "FK_Usuario_Perfil_PerfilId",
                table: "Usuario");

            migrationBuilder.DropPrimaryKey(
                name: "PK_Perfil",
                table: "Perfil");

            migrationBuilder.RenameTable(
                name: "Perfil",
                newName: "MyProperty");

            migrationBuilder.AddColumn<string>(
                name: "SenhaConfere",
                table: "Usuario",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SenhaNova",
                table: "Usuario",
                nullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_MyProperty",
                table: "MyProperty",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_PerfilPermissao_MyProperty_PerfilId",
                table: "PerfilPermissao",
                column: "PerfilId",
                principalTable: "MyProperty",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_Usuario_MyProperty_PerfilId",
                table: "Usuario",
                column: "PerfilId",
                principalTable: "MyProperty",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
