﻿using Infra.Utils.Paginacao.Model;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Utils.Paginacao
{
    public class Paginacao<T> where T : class
    {
        public int TotalItens { get; set; }
        public int TotalPaginas { get; set; }
        public int TamanhoPagina { get; set; }
        public int NumeroPagina { get; set; }
        public IList<T> ListaItens { get; set; }

        public static Paginacao<T> From(ObjetoPaginacao param, IQueryable<T> origens)
        {
            if (param == null) param = new ObjetoPaginacao();

            int _Total = origens.Count();
            int _TotalPaginas = (int)Math.Ceiling(_Total / (double)param.Tamanho);
            return new Paginacao<T>
            {
                TotalItens = _Total,
                TotalPaginas = _TotalPaginas,
                TamanhoPagina = param.Tamanho,
                NumeroPagina = param.Pagina,
                ListaItens = origens.Skip(param.QtdParaDescartar).Take(param.Tamanho).ToList()
            };
        }
    }
}
