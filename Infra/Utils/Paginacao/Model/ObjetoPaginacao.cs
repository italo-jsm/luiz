﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Utils.Paginacao.Model
{
    public class ObjetoPaginacao
    {
        private readonly int Tamanho_Padrao = 25;
        private int _pagina = 0;
        private int _tamanho = 0;

        public int Pagina
        {
            get
            {
                return (_pagina <= 0) ? 1 : _pagina;
            }
            set
            {
                _pagina = value;
            }
        }
        public int Tamanho
        {
            get
            {
                return (_tamanho <= 0) ? Tamanho_Padrao : _tamanho;
            }
            set
            {
                _tamanho = value;
            }
        }
        public int QtdParaDescartar => Pagina > 0 ? Tamanho * (Pagina - 1) : Tamanho;
    }
}
