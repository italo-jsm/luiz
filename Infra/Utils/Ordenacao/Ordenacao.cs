using System.Linq;
using Infra.Models.Database;
using System.Linq.Dynamic.Core;
using Infra.Utils.Ordenacao.Model;

namespace Infra.Utils.Ordenacao
{
    public static class Order
    {
        public static IQueryable<Cliente> AplicaOrdenacaoCliente(this IQueryable<Cliente> query, Ordem ordem)
        {
            if (!string.IsNullOrWhiteSpace(ordem.OrdenaPor))
            {
                query = query.OrderBy(ordem.OrdenaPor);
            }
            return query;
        }

        public static IQueryable<Produto> AplicaOrdenacaoProduto(this IQueryable<Produto> query, Ordem ordem)
        {
            if(!string.IsNullOrWhiteSpace(ordem.OrdenaPor))
            {
                query = query.OrderBy(ordem.OrdenaPor);
            }
            return query;
        }

        public static IQueryable<Usuario> AplicaOrdenacaoUsuario(this IQueryable<Usuario> query, Ordem ordem)
        {
            if(!string.IsNullOrWhiteSpace(ordem.OrdenaPor))
            {
                query = query.OrderBy(ordem.OrdenaPor);
            }
            return query;
        }

        public static IQueryable<Estoque> AplicaOrdenacaoEstoque(this IQueryable<Estoque> query, Ordem ordem)
        {
            if(!string.IsNullOrWhiteSpace(ordem.OrdenaPor))
            {
                query = query.OrderBy(ordem.OrdenaPor);
            }
            return query;
        }

        public static IQueryable<Venda> AplicaOrdenacaoVenda(this IQueryable<Venda> query, Ordem ordem)
        {
            if(!string.IsNullOrWhiteSpace(ordem.OrdenaPor))
            {
                query = query.OrderBy(ordem.OrdenaPor);
            }
            return query;
        }

        public static IQueryable<Vendedor> AplicaOrdenacaoVendedor(this IQueryable<Vendedor> query, Ordem ordem)
        {
            if(!string.IsNullOrWhiteSpace(ordem.OrdenaPor))
            {
                query = query.OrderBy(ordem.OrdenaPor);
            }
            return query;
        }

        public static IQueryable<Perfil> AplicaOrdenacaoPerfil(this IQueryable<Perfil> query, Ordem ordem) {
            if(!string.IsNullOrWhiteSpace(ordem.OrdenaPor)) {
                query = query.OrderBy(ordem.OrdenaPor);
            }
            return query;
        }
    }
}
