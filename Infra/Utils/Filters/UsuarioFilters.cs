﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Utils.Filters
{
    public class UsuarioFilters 
    {
        public string NomeCompleto { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
    }
}
