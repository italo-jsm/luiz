﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Utils.Filters
{
    public class ClienteFilters
    {
        public string RazaoSocial { get; set; }
        public string DocumentoIdentificacao { get; set; }
    }
}
