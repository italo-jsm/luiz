namespace Infra.Utils.Filters
{
    public class ProdutoFilters
    {
        public string Nome { get; set; }
        public string Fabricante { get; set; }
        public long Id { get; set; }
    }
}