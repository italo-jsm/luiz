﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Utils.Filters
{
    public class EstoqueFilters
    {
        public string Nome { get; set; }
        public string Quantidade { get; set; }
        public string Valor { get; set; }
    }
}
