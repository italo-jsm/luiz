using Infra.Models.Database;
using Infra.Utils.Filters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Extensions
{
    public static class VendedorExtentions
    {
        public static IQueryable<Vendedor> VendedorFiltro(this IQueryable<Vendedor> query, VendedorFilters filtro)
        {
            if(filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.Codigo))
                   query = query.Where(x => x.Codigo.Equals(filtro.Codigo));
                if (!string.IsNullOrWhiteSpace(filtro.Departamento))
                    query = query.Where(x => x.Departamento.Equals(filtro.Departamento));
            }
            return query;
        }
    }
}
