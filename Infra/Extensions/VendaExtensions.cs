using Infra.Models.Database;
using Infra.Utils.Filters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Extensions
{
    public static class VendaExtentions
    {
        public static IQueryable<Venda> VendaFiltro(this IQueryable<Venda> query, VendaFilters filtro)
        {
            query = query.Include(x => x.Vendedor);
            if(filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.Quantidade))
                   query = query.Where(x => x.Quantidade.Equals(filtro.Quantidade));
                if (!string.IsNullOrWhiteSpace(filtro.ValorTotal))
                    query = query.Where(x => x.ValorTotal.Equals(filtro.ValorTotal));
            }
            return query;
        }
    }
}
