﻿using Infra.Models.Database;
using Infra.Utils.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Extensions
{
    public static class EstoqueExtensions
    {

        public static IQueryable<Estoque> EstoqueFiltro(this IQueryable<Estoque> query, EstoqueFilters filtro)
        {
            if (filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.Nome))
                    query = query.Where(x => x.Produto.Nome.Contains(filtro.Nome));
                if (!string.IsNullOrWhiteSpace(filtro.Quantidade))
                    query = query.Where(x => x.Quantidade.Equals(filtro.Quantidade));
                if (!string.IsNullOrWhiteSpace(filtro.Valor))
                    query = query.Where(x => x.Produto.Valor.Equals(filtro.Valor));
            }
            return query;
        }
    }
}
