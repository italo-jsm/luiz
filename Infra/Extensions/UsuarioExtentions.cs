﻿using Infra.Models.Database;
using Infra.Utils.Filters;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Extensions
{
    public static class UsuarioExtentions
    {
        public static IQueryable<Usuario> UsuarioFiltro(this IQueryable<Usuario> query, UsuarioFilters filtro)
        {
            query = query.Include(x => x.Perfil);
            if(filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.NomeCompleto))
                    query = query.Where(x => x.NomeCompleto.Equals(filtro.NomeCompleto));
                if (!string.IsNullOrWhiteSpace(filtro.Email))
                    query = query.Where(x => x.Email.Equals(filtro.Email));
                if (!string.IsNullOrWhiteSpace(filtro.Cpf))
                    query = query.Where(x => x.Cpf.Equals(filtro.Cpf));
            }
            return query;
        }
    }
}
