using Infra.Models.Database;
using Infra.Utils.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Infra.Extensions
{
    public static class PerfilExtensions
    {

        public static IQueryable<Perfil> PerfilFiltro(this IQueryable<Perfil> query, PerfilFilters filtro)
        {
            if (filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.Nome))
                    query = query.Where(x => x.Nome.Contains(filtro.Nome));
            }
            return query;
        }
    }
}
