using System.Linq;
using Infra.Models.Database;
using Infra.Utils.Filters;

namespace Infra.Extensions
{
    public static class ProdutoExtensions
    {
        public static IQueryable<Produto> ProdutoFiltro(this IQueryable<Produto> query, ProdutoFilters filtro)
        {
            if (filtro != null)
            {
                if (!string.IsNullOrWhiteSpace(filtro.Nome))
                    query = query.Where(x => x.Nome.Equals(filtro.Nome));
                if (!string.IsNullOrWhiteSpace(filtro.Fabricante))
                    query = query.Where(x => x.Fabricante.Equals(filtro.Fabricante));
                if (filtro.Id > 0)
                    query = query.Where(x => x.Id == filtro.Id);
            }
            return query;
        }
    }
}