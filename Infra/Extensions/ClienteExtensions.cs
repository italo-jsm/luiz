﻿using Infra.Models.Database;
using Infra.Utils.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.EntityFrameworkCore;

namespace Infra.Extensions
{
    public static class ClienteExtensions
    {
        //TODO: Implementar extension, para transformar model do banco em DTO e vice versa
        //public static ClienteDTO ToClienteDTO(this Cliente cliente)
        //{

        //}

        //public static Cliente ToCliente(this ClienteDTO clienteDTO)
        //{

        //}

        public static IQueryable<Cliente> ClienteFiltros(this IQueryable<Cliente> query, ClienteFilters filtro)
        {
            query = query
                .Include(x => x.Endereco)
                .Include(x => x.Representante);

            if(filtro != null)
            {
                if (!String.IsNullOrWhiteSpace(filtro.RazaoSocial))
                {
                    query = query.Where(r => r.RazaoSocial.Equals(filtro.RazaoSocial));
                }
                if (!String.IsNullOrWhiteSpace(filtro.DocumentoIdentificacao))
                {
                    query = query.Where(d => d.DocumentoIdentificacao.Equals(filtro.DocumentoIdentificacao));
                }
            }
            return query;
        }
    }
}
