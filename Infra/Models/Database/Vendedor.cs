﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Database
{
    public class Vendedor : ModeloAbstrato
    {
        public string NomeCompleto { get; set; }
        public string Cpf { get; set; }
        public long Codigo { get; set; }
        public string Departamento { get; set; }
    }
}
