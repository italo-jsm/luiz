﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Database
{
    public class Contato : ModeloAbstrato
    {
        public string NomeCompleto { get; set; }
        public string Cargo { get; set; }
        public string DocumentoIdentificacao { get; set; }
        public string Telefone { get; set; }
        public string Celular { get; set; }
    }
}
