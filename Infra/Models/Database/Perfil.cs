﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infra.Models.Database
{
    public class Perfil : ModeloAbstrato
    {
        [Required]
        public string Nome { get; set; }
        public string Descricao { get; set; }
    }
}
