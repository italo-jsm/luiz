﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Infra.Models.Database
{
    public class Usuario : ModeloAbstrato
    {
        public string NomeCompleto { get; set; }
        public string Cpf { get; set; }
        public string Email { get; set; }
        public string Senha { get; set; }
        [NotMapped]
        public string SenhaNova { get; set; }
        [NotMapped]
        public string SenhaConfere { get; set; }
        public virtual Perfil Perfil { get; set; }
    }
}
