﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Database
{
    public class Cliente : ModeloAbstrato
    {
        public string RazaoSocial { get; set; }
        public string DocumentoIdentificacao { get; set; }
        public virtual Contato Representante { get; set; }
        public virtual Endereco Endereco { get; set; }
    }
}
