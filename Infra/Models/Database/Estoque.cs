﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Database
{
    public class Estoque
    {
        public long Id { get; set; }
        public long Quantidade { get; set; }
        public Produto Produto { get; set; }
    }
}
