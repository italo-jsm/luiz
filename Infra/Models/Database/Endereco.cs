﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Database
{
    public class Endereco : ModeloAbstrato
    {
        public string Logradouro { get; set; }
        public string Bairro { get; set; }
        public long Numero { get; set; }
        public string Cidade { get; set; }
        public string Estado { get; set; }
    }
}
