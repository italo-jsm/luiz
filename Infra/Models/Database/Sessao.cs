﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;

namespace Infra.Models.Database
{
    public class Sessao
    {
        [Key]
        public string Chave { get; set; }
        public DateTimeOffset Validade { get; set; }
        public DateTimeOffset Data { get; set; }
        public bool Ativo { get; set; }
        public virtual Usuario Usuario { get; set; }
    }
}
