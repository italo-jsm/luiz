﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.Database
{
    public abstract class ModeloAbstrato
    {
        public long Id { get; set; }
        public DateTimeOffset DataCriacao { get; set; }
        public DateTimeOffset DataModificacao { get; set; }
        public long ResponsavelCriacao { get; set; }
        public long ResponsavelModificacao { get; set; }
        public bool Ativo { get; set; }
    }
}
