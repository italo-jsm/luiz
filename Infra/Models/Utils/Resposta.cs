﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net;
using System.Text;

namespace Infra.Models.Utils
{
    public class Resposta
    {
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public HttpStatusCode Status { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public string Descricao { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public long Pagina { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public long QtdDePaginas { get; set; }
        [JsonProperty(DefaultValueHandling = DefaultValueHandling.Ignore)]
        public object Resultado { get; set; }

        public static Resposta Forbidden()
        {
            return new Resposta
            {
                Status = HttpStatusCode.Forbidden,
                Descricao = "Acesso não autorizado."
            };
        }

        public static Resposta Created()
        {
            return new Resposta
            {
                Status = HttpStatusCode.Created,
                Descricao = "Adicionado com sucesso."
            };
        }

        public static Resposta BadRequest()
        {
            return new Resposta
            {
                Status = HttpStatusCode.BadRequest,
                Descricao = "Não foi possível processar a requisição."
            };
        }

        public static Resposta BadRequest(string descricao)
        {
            return new Resposta
            {
                Status = HttpStatusCode.BadRequest,
                Descricao = descricao
            };
        }

        public static Resposta NoContent(string nome, string tipo)
        {
            Resposta r = new Resposta();
            switch (tipo)
            {
                case "f":
                    r.Status = HttpStatusCode.NoContent;
                    r.Descricao = nome + " não encontrada.";
                    break;
                case "m":
                    r.Status = HttpStatusCode.NoContent;
                    r.Descricao = nome + " não encontrado.";
                    break;
                case "pluralf":
                    r.Status = HttpStatusCode.NoContent;
                    r.Descricao = nome + " não encontradas.";
                    break;
                case "pluralm":
                    r.Status = HttpStatusCode.NoContent;
                    r.Descricao = nome + " não encontrados.";
                    break;
                case "M":
                    r.Status = HttpStatusCode.NoContent;
                    r.Descricao = "Nenhum " + nome + " encontrado";
                    break;
                case "F":
                    r.Status = HttpStatusCode.NoContent;
                    r.Descricao = "Nenhuma " + nome + " encontrada";
                    break;
            }

            return r;
        }

        public static Resposta Found(string nome, string tipo)
        {
            Resposta r = new Resposta();
            switch (tipo)
            {
                case "f":
                    r.Status = HttpStatusCode.Found;
                    r.Descricao = nome + " encontrada com sucesso.";
                    break;
                case "m":
                    r.Status = HttpStatusCode.Found;
                    r.Descricao = nome + " encontrado com sucesso.";
                    break;
                case "pluralf":
                    r.Status = HttpStatusCode.Found;
                    r.Descricao = nome + " encontradas com sucesso.";
                    break;
                case "pluralm":
                    r.Status = HttpStatusCode.Found;
                    r.Descricao = nome + " encontrados com sucesso.";
                    break;
                case "updateM":
                    r.Status = HttpStatusCode.Found;
                    r.Descricao = nome + " atualizado com sucesso.";
                    break;
                case "updateF":
                    r.Status = HttpStatusCode.Found;
                    r.Descricao = nome + " atualizada com sucesso.";
                    break;
            }
            return r;
        }

        public static Resposta DataIsComplete()
        {
            return new Resposta
            {
                Status = HttpStatusCode.OK,
                Descricao = "Todos estão dados corretos."
            };
        }

        public static Resposta DataIsEmpty()
        {
            return new Resposta
            {
                Status = HttpStatusCode.BadRequest,
                Descricao = "Todos os dados não foram informados."
            };
        }

        public static Resposta Ok(string descricao = "")
        {
            return new Resposta
            {
                Status = HttpStatusCode.OK,
                Descricao = descricao
            };
        }
    }
}
