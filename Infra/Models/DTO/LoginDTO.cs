﻿using Infra.Models.Database;
using System;
using System.Collections.Generic;
using System.Text;

namespace Infra.Models.DTO
{
    public class LoginDTO
    {
        public long Id { get; set; }
        public string Nome { get; set; }
        public Perfil Perfil { get; set; }
        public string Token { get; set; }
        public string Mensagem { get; set; }
    }
}
